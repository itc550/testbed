import platform
from time import sleep
import bluetooth._bluetooth as bluez
from random import randint
import random
import os
import time

from bluetooth_utils import (start_le_advertising, stop_le_advertising)
from bluetooth_utils import (toggle_device, enable_le_scan, parse_le_advertising_events, disable_le_scan, raw_packet_to_str)

# Platform stuff and GPIO
platform_check=platform.platform()
platform_ref = "Linux-4.14.71-v7+-armv7l-with-debian-9.4"
print(platform_check)
if platform_check == platform_ref:
    print("Setting up GPIO")
    import RPi.GPIO as GPIO
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(3, GPIO.OUT)
    GPIO.output(3, GPIO.HIGH)


def unalign(mini=2, maxi=58):
    rand = os.urandom(10)
    random.seed(int(rand.hex(), 16))
    sleep(random.uniform(mini, maxi))


def socsim(soc):
    soc -= randint(1, 2)
    return soc


def status(soc, ref):
    if soc >= ref:
        if platform_check == platform_ref:
            GPIO.output(3, GPIO.HIGH)
        return True
    if soc < ref:
        if platform_check == platform_ref:
            GPIO.output(3, GPIO.LOW)
        return False


def le_advertise_packet_handler(mac, data, rssi):
    global prev_data
    global ref
    global statusval
    global old_ref
    if mac_addr == mac:
        data_str = raw_packet_to_str(data)
        data_str = int(str(data_str[2:]), 16)
        ref = data_str
        print(data_str)


def burst(data, period=0.250, interval=32):
    try:
        print("Sending: "+str(data))
        start_le_advertising(sock, min_interval=interval, max_interval=interval, data=(int(data),))
        sleep(period)
        stop_le_advertising(sock)
    except:
        stop_le_advertising(sock)
        disable_le_scan(sock)
        GPIO.cleanup()
        raise


def continuous(data, interval=16000):
    try:
        print("Sending: "+str(data))
        start_le_advertising(sock, min_interval=interval, max_interval=interval, data=(int(data),))
    except:
        stop_le_advertising(sock)
        disable_le_scan(sock)
        GPIO.cleanup()
        raise


def cycle(ref_time, mtype, data):
    if mtype == 1:
        continuous(data,300)
    if mtype == 2:
        burst(data)
    burst(data)
    while ref_time >= time.time():
        parse_le_advertising_events(1, sock, handler=le_advertise_packet_handler, debug=False)


# Main ¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤

mac_addr = "B8:27:EB:62:AF:40"  # controller address
mac_name = "Controller"  # Controller name

ref = 0
soc = 100
old_ref = ref
statusval = True

# Bluetooth setup
dev_id = 0  # the bluetooth device is hci0
try:
    sock = bluez.hci_open_dev(dev_id)  # open bluez hci device
except:
    print("Cannot open bluetooth device %i" % dev_id)
    raise
toggle_device(dev_id, True)  # turn on device
enable_le_scan(sock, 100, 100, filter_duplicates=False)  # enable scanning with device

# Setup of time stuff
unalign()
stid = time.time()

# Cycle for x time or # of times
try:
    for i in range(0, 100):
        if statusval:
            soc = socsim(soc)
        stid += 2
        cycle(stid, 1, 2)
        i = i + 1
        if ref != old_ref:
            statusval = status(soc, ref)
            old_ref = ref

        #print("Latest reference: ", ref)
        #print("Charging status:", statusval)

except: #For interrupts or exceptions
    stop_le_advertising(sock)
    disable_le_scan(sock)
    GPIO.cleanup()
    raise

# Clean up
stop_le_advertising(sock)
disable_le_scan(sock)
GPIO.cleanup()
