int modtager = 6;
long count = 0;
long timeStamp = 0;

void setup() {
  Serial.begin(9600);
  pinMode(modtager, INPUT_PULLUP);
}

void loop() {

  if (digitalRead(modtager) == 0) {
    timeStamp = micros();
    while (timeStamp+120000000 > micros()) {
      if (digitalRead(modtager) == 0) {
        count++;
        Serial.print(micros()-timeStamp);
        Serial.print(";");
        Serial.println(count);
        while(digitalRead(modtager) == 0) {
          }
      }
    }
    while (1) {}
  }
}
