import platform
from time import sleep
import time
import bluetooth._bluetooth as bluez
import random
import os
import sys

from bluetooth_utils import *

id = sys.argv[1]

platform_check=platform.platform()
platform_ref="Linux-4.14.71-v7+-armv7l-with-debian-9.4"
print(platform_check)
mac_addr="B8:27:EB:9B:F4:40"
mac_name="Controller"
ref = 0
dev_id = 0  # the bluetooth device is hci0
soc = 100
old_ref = ref
statusval = True
rand = os.urandom(10)
random.seed(int(rand.hex(), 16))
sleep(random.uniform(2,55))


try:
    sock = bluez.hci_open_dev(dev_id)
except:
    print("Cannot open bluetooth device %i" % dev_id)
    raise



toggle_device(dev_id, True)
#enable_le_scan(sock,0x05,0x05, filter_duplicates=False)


startTid = time.time()
try:
    for i in range(90):

        for j in range(10):

            print("Sending ",str(j))
            start_le_advertising(sock, min_interval=32, max_interval=32, data=((j*10)+int(id),))
            sleep(0.02)
            stop_le_advertising(sock)

        while time.time() <= startTid+60:
            pass

        startTid+=60

except:
    stop_le_advertising(sock)
    disable_le_scan(sock)

    raise

stop_le_advertising(sock)
disable_le_scan(sock)
