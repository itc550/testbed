import platform
from time import sleep
import time
import bluetooth._bluetooth as bluez

from bluetooth_utils import (start_le_advertising,
                             stop_le_advertising)

from bluetooth_utils import (toggle_device,
                             enable_le_scan, parse_le_advertising_events,
                             disable_le_scan, raw_packet_to_str)


platform_check=platform.platform()
platform_ref="Linux-4.14.71-v7+-armv7l-with-debian-9.4"
print(platform_check)
mac_addr="B8:27:EB:BE:03:43"
mac_name="Sender"
dev_id = 0  # the bluetooth device is hci0

if (platform_check == platform_ref):
    print("Setting up GPIO")
    import RPi.GPIO as GPIO

    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(2, GPIO.OUT)
    GPIO.output(2, GPIO.HIGH)



try:
    sock = bluez.hci_open_dev(dev_id)
except:
    print("Cannot open bluetooth device %i" % dev_id)
    raise


def le_advertise_packet_handler(mac, data, rssi):
    global start_time
    global state
    data_str = raw_packet_to_str(data)
    data_str = int(str(data_str[2:]), 16)
    if (mac_addr == mac or mac=="B8:27:EB:AC:C8:6E" or mac=="9C:B6:D0:B8:08:E4"):
        if (platform_check == platform_ref):
            GPIO.output(2, GPIO.LOW)
        print(mac_name + ": %s %s" % (mac, data_str))
        start_time = time.time()
        state=0


toggle_device(dev_id, True)
sleep(5)
enable_le_scan(sock, 0x25, 0x25, filter_duplicates=False)

start_time = time.time()
state=0

try:
    while(1):
        parse_le_advertising_events(0.1, sock,
                                    handler=le_advertise_packet_handler,
                                    debug=False)
        #print(time.time() - start_time)
        if (time.time() - start_time >= 2.5 and state==0):
            if (platform_check == platform_ref):
                GPIO.output(2, GPIO.HIGH)
            print("\n", time.time() - start_time, "\n")
            state=1
            sleep(1)



except:
    disable_le_scan(sock)
    GPIO.cleanup()
    raise
disable_le_scan(sock)
GPIO.cleanup()