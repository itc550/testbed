int sender = 6;
int modtager = 7;
unsigned long startTime = 0;
unsigned long elapsedTime = 0;
int status1 = 0;

void setup() {
  Serial.begin(9600);
  pinMode(sender, INPUT_PULLUP);
  pinMode(modtager, INPUT_PULLUP);
}

void loop() {
  if (digitalRead(sender) != 1 && digitalRead(modtager)==1 && status1 == 0) {
    startTime = micros();
    //Serial.println("den ene er tændt");
    status1 = 1;
  }
  if(digitalRead(sender) == 0 && digitalRead(modtager)==0 && status1 == 1){
    elapsedTime = micros() - startTime;
    Serial.println(elapsedTime);
    status1 = 2;
  }
    if(digitalRead(sender) == 1 && digitalRead(modtager)==1 && status1 == 2){
      //Serial.println("---------------");
    status1 = 0;
  }
}
