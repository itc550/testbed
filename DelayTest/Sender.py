import platform
from time import sleep
import bluetooth._bluetooth as bluez

from bluetooth_utils import (start_le_advertising,
                             stop_le_advertising)

from bluetooth_utils import (toggle_device,
                             enable_le_scan, parse_le_advertising_events,
                             disable_le_scan, raw_packet_to_str)


platform_check=platform.platform()
platform_ref="Linux-4.14.71-v7+-armv7l-with-debian-9.4"
print(platform_check)


dev_id = 0  # the bluetooth device is hci0




if (platform_check==platform_ref):
    print("Setting up GPIO")
    import RPi.GPIO as GPIO

    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(2, GPIO.OUT)
    GPIO.output(2, GPIO.HIGH)



try:
    sock = bluez.hci_open_dev(dev_id)
except:
    print("Cannot open bluetooth device %i" % dev_id)
    raise

sleep(10)
try:
        for i in range(0,100):
            start_le_advertising(sock,
                                 min_interval=32, max_interval=32,
                                 data= (100,))
            if (platform_check == platform_ref):
                GPIO.output(2, GPIO.LOW)
            print("Sending: ", i+1," out of 100")
            sleep(5)
            stop_le_advertising(sock)
            if (platform_check == platform_ref):
                GPIO.output(2, GPIO.HIGH)
            print("Not Sending...")
            sleep(5)

except:
    stop_le_advertising(sock)
    GPIO.cleanup()
    raise

stop_le_advertising(sock)
GPIO.cleanup()