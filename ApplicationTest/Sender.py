import platform
from time import sleep
import time
import bluetooth._bluetooth as bluez
from bluetooth_utils import (start_le_advertising,
                             stop_le_advertising)

platform_check=platform.platform()
platform_ref="Linux-4.14.71-v7+-armv7l-with-debian-9.4"
print(platform_check)

#Time between packets in sec
interval=10
#10*1000/0,625=16000
packet_freq=16000
#Number of packet to be sent
packet_number=10

dev_id = 0  # the bluetooth device is hci0

def send_packet(sock,data):
    start_le_advertising(sock, min_interval=packet_freq, max_interval=packet_freq, data=(data,))
    print("Advertising packet: " + str(data) + "\nTime is: " + str(time.time()) + ", sleeping now ZZZZzzzz...\n")
    #bluez.cmd_opcode_pack(0x03,0x0008)
    sleep(1)
    stop_le_advertising(sock)


try:
    sock = bluez.hci_open_dev(dev_id)
except:
    print("Cannot open bluetooth device %i" % dev_id)
    raise

start_time = time.time()
start_time_ref = start_time
print("Time reference point is: "+str(start_time)+"\nWill advertise every "+str(interval)+" secs")
print("For at total of "+str(packet_number)+" times \n")
i=1

try:
    print("Clearing BT buffer")
    start_le_advertising(sock, min_interval=300, max_interval=300, data=(0,))
    sleep(2.5)
    stop_le_advertising(sock)
    print("Stopped clearing BT buffer\n")
    sleep(2.5)

    start_le_advertising(sock, min_interval=packet_freq, max_interval=packet_freq, data=(i,))
    while(start_time_ref+(interval*packet_number)-1>=start_time):
        if time.time() - start_time >= interval:
            send_packet(sock,i)
            start_time = start_time+interval
            i = i+1


except:
    stop_le_advertising(sock)
    raise

stop_le_advertising(sock)