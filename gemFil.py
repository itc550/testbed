import serial
import csv
import os

ser = serial.Serial('/dev/ttyACM0', 9600)
ser.flushInput()

i = 0
while os.path.exists("test_data_%s.csv" % i):
	i += 1

while True:
    try:
        ser_bytes = ser.readline()
        decoded_bytes = (ser_bytes[0:len(ser_bytes)-2].decode("utf-8"))
        print(decoded_bytes)
        with open("test_data_%s.csv" % i, "a") as f:
            writer = csv.writer(f, delimiter=",")
            writer.writerow([decoded_bytes])

    except:
        print("Keyboard Interrupt")
        break
