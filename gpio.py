import RPi.GPIO as GPIO
import time

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(2, GPIO.OUT)

timeout = time.time() + 60

while True:
	test = 0
	if test == 5 or time.time() > timeout:
		break
	test = test - 1
	GPIO.output(2, GPIO.LOW)
	time.sleep(1)
	GPIO.output(2, GPIO.HIGH)
	time.sleep(1)

GPIO.cleanup()
