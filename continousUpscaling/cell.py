import platform
from time import sleep
import bluetooth._bluetooth as bluez
import random
import sys
import os
import time

from bluetooth_utils import *

interval = 60000    #mili secounds




ID = int(sys.argv[1])
rand = os.urandom(10)
random.seed(int(rand.hex(), 16))
sleep(random.uniform(0.001,0.1))
platform_check=platform.platform()
platform_ref="Linux-4.14.71-v7+-armv7l-with-debian-9.4"
print(platform_check)
mac_addr="B8:27:EB:9B:F4:40"
mac_name="Controller"
ref = 0
dev_id = 0  # the bluetooth device is hci0
soc = 100
old_ref = ref
statusval = True



interval /= 10
send_interval = int(interval/0.625)
min_send_interval = int(send_interval)
tidint = interval/1000
j = 0

print(send_interval)

try:
    sock = bluez.hci_open_dev(dev_id)
except:
    print("Cannot open bluetooth device %i" % dev_id)
    raise





toggle_device(dev_id, True)
#enable_le_scan(sock,0x05,0x05, filter_duplicates=False)
try:
    start_le_advertising(sock,
                         min_interval=send_interval, max_interval=send_interval, adv_type=0,
                         data=(ID,))


    startTid = time.time()
    while True:

        sendid = j*10+ID
        update_le_adv_data(sock, data=(sendid,))

        j += 1
        if j >= 10:
            j = 0

        while(time.time() <= startTid + tidint):
            pass
        startTid += tidint




except:
    stop_le_advertising(sock)
    disable_le_scan(sock)

    raise

stop_le_advertising(sock)
disable_le_scan(sock)
