import platform
from time import sleep
import bluetooth._bluetooth as bluez
import sys
from bluetooth_utils import (start_le_advertising,
                             stop_le_advertising)

platform_check=platform.platform()
platform_ref="Linux-4.14.71-v7+-armv7l-with-debian-9.4"
print(platform_check)

dev_id = 0  # the bluetooth device is hci0

try:
    sock = bluez.hci_open_dev(dev_id)
except:
    print("Cannot open bluetooth device %i" % dev_id)
    raise

try:
        start_le_advertising(sock,
                             min_interval=int(400), max_interval=int(400), adv_type=0,
                             data= (100,))
        print("Using min_interval: 400" + " Using max_interval: 400")
        sleep(1000)

except:
    stop_le_advertising(sock)
    raise

stop_le_advertising(sock)
