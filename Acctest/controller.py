"""
Simple BLE advertisement example
"""
import platform
from time import sleep
import bluetooth._bluetooth as bluez
import csv
import os
import time
import sys

from bluetooth_utils import (start_le_advertising,
                             stop_le_advertising)

from bluetooth_utils import (toggle_device,
                             enable_le_scan, parse_le_advertising_events,
                             disable_le_scan, raw_packet_to_str)


def burst(data, period=0.250, interval=32):
    try:
        print("Sending: "+str(data))
        start_le_advertising(sock, min_interval=interval, max_interval=interval, data=(int(data),))
        sleep(period)
        stop_le_advertising(sock)
    except:
        stop_le_advertising(sock)
        disable_le_scan(sock)
        GPIO.cleanup()
        raise


def continuous(data, interval=16000):
    try:
        print("Sending: "+str(data))
        start_le_advertising(sock, min_interval=interval, max_interval=interval, data=(int(data),))
    except:
        stop_le_advertising(sock)
        disable_le_scan(sock)
        GPIO.cleanup()
        raise


def cycle(ref_time, mtype, data):
    if mtype == 1:
        continuous(data,300)
    if mtype == 2:
        burst(data)
    burst(data)
    while ref_time >= time.time():
        parse_le_advertising_events(1, sock, handler=le_advertise_packet_handler, debug=False)



dev_id = 0  # the bluetooth device is hci0
toggle_device(dev_id, True)

try:
    sock = bluez.hci_open_dev(dev_id)
except:
    print("Cannot open bluetooth device %i" % dev_id)
    raise
mac_addr="B8:27:EB:86:DB:84",   "B8:27:EB:10:84:75",   "B8:27:EB:28:5F:0F",     "B8:27:EB:0E:E2:B7",    "B8:27:EB:BF:60:E3",    "B8:27:EB:BE:03:43",    "B8:27:EB:AC:C8:6E",    "B8:27:EB:62:AF:40",    "B8:27:EB:65:27:F3",    "B8:27:EB:C4:B9:C0",     "48:51:B7:37:B1:34"#,   "9C:B6:D0:B8:08:E4",
mac_name="Rpi 2",               "Rpi 3",               "Rpi 4",                 "Rpi 5",                "Rpi 6",                "Rpi 7",                "Rpi 8",                "Rpi 9",                "Rpi 10",              "Rpi 11",                "Stempeldiller"#,       "Rolf"
active_addr=[]
toggle=[0]


enable_le_scan(sock, 100, 100, filter_duplicates=False)

q=1
start_time = time.time()
try:
        while True:
            
            continuous(1,300)
            sleep(2)
            #stop_le_advertising(sock)


except:
    stop_le_advertising(sock)
    disable_le_scan(sock)

    raise

stop_le_advertising(sock)
disable_le_scan(sock)

