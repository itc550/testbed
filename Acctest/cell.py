import platform
from time import sleep
import bluetooth._bluetooth as bluez
import random
import sys
import os
import time

from bluetooth_utils import *

interval = 10000    #mili secounds




#ID = int(sys.argv[1])
rand = os.urandom(10)
random.seed(int(rand.hex(), 16))
sleep(random.uniform(0.001,0.1))
platform_check=platform.platform()
platform_ref="Linux-4.14.71-v7+-armv7l-with-debian-9.4"
print(platform_check)
mac_addr="B8:27:EB:9B:F4:40"
mac_name="Controller"
ref = 0
dev_id = 0  # the bluetooth device is hci0
soc = 100
old_ref = ref
statusval = True



interval /= 10
send_interval = int(interval/0.625)
min_send_interval = int(send_interval)
tidint = interval/1000
a = 0


def le_advertise_packet_handler(mac, data, rssi):
    global a
    global prev_data
    data_str = raw_packet_to_str(data)
    data_str = int(str(data_str[2:]), 16)
    if (mac=="B8:27:EB:9B:F4:40"):
        a += 1
        #print("     New MAC address: ",mac)


#print(send_interval)

try:
    sock = bluez.hci_open_dev(dev_id)
except:
    print("Cannot open bluetooth device %i" % dev_id)
    raise





toggle_device(dev_id, True)
enable_le_scan(sock,100,100, filter_duplicates=False)
try:



    startTid = time.time()
    for i in range (20):

        parse_le_advertising_events(60, sock,
                                        handler=le_advertise_packet_handler,
                                        debug=False)
        

        print(a)
        a=0
    print('done')


except:
    stop_le_advertising(sock)
    disable_le_scan(sock)

    raise

stop_le_advertising(sock)
disable_le_scan(sock)
