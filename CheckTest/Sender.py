import platform
from time import sleep
import bluetooth._bluetooth as bluez
import sys
from bluetooth_utils import (start_le_advertising,
                             stop_le_advertising)

platform_check=platform.platform()
platform_ref="Linux-4.14.71-v7+-armv7l-with-debian-9.4"
print(platform_check)

dev_id = 0  # the bluetooth device is hci0
param1 = sys.argv[1]
param2 = sys.argv[2]
i = 0;

try:
    sock = bluez.hci_open_dev(dev_id)
except:
    print("Cannot open bluetooth device %i" % dev_id)
    raise

try:
    while 1:
        start_le_advertising(sock,
                             min_interval=int(param1), max_interval=int(param2),
                             data= (i,))
        print("Using min_interval: "+ str(param1) + " Using max_interval: " + str(param2))
        sleep(0.8)
        i += 1

except:
    stop_le_advertising(sock)
    raise

stop_le_advertising(sock)
