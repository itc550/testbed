import platform
from time import sleep
import time
import bluetooth._bluetooth as bluez

from bluetooth_utils import (start_le_advertising,
                             stop_le_advertising)

from bluetooth_utils import (toggle_device,
                             enable_le_scan, parse_le_advertising_events,
                             disable_le_scan, raw_packet_to_str)
import sys

platform_check=platform.platform()
platform_ref="Linux-4.14.71-v7+-armv7l-with-debian-9.4"
print(platform_check)
mac_name="Sender"
dev_id = 0  # the bluetooth device is hci0
param1 = sys.argv[1]
param2 = sys.argv[2]
param3 = sys.argv[3]
counter = 0

if (platform_check == platform_ref):
    print("Setting up GPIO")
    import RPi.GPIO as GPIO
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(2, GPIO.OUT)
    GPIO.output(2, GPIO.HIGH)

try:
    sock = bluez.hci_open_dev(dev_id)
except:
    print("Cannot open bluetooth device %i" % dev_id)
    raise


def le_advertise_packet_handler(mac, data, rssi):
    global start_time
    global counter
    data_str = raw_packet_to_str(data)
    data_str = int(str(data_str[2:]), 16)
    #if (mac=="B8:27:EB:10:84:75"):
    if (mac == "B8:27:EB:62:AF:40"):
        if (platform_check == platform_ref):
            GPIO.output(2, GPIO.LOW)
        counter = counter + 1
        print(mac_name + ": %s %s" % (mac, data_str))
        if (platform_check == platform_ref):
            GPIO.output(2, GPIO.HIGH)

toggle_device(dev_id, True)
enable_le_scan(sock, int(param1), int(param2), filter_duplicates=False)

try:
    while(1):
        parse_le_advertising_events(int(param3), sock,
                                    handler=le_advertise_packet_handler,
                                    debug=False)


except:
    disable_le_scan(sock)
    GPIO.cleanup()
    raise
disable_le_scan(sock)
GPIO.cleanup()
